﻿using System;
using System.Collections.Generic;

namespace Cduc
{
    public static class Program
    {
        public static void Main(string[] args)
        {
           Random random = new Random(); 

var isRunning = true;

while (isRunning) {
    var isReset = false;
    bool isBadClassChoice;
    var selectedClass = Classes.Héros;
    var map = new Map();
    var etage = 1;
    
    
    do {
        Console.Clear();
        
        isBadClassChoice = false;

        Console.WriteLine("Veuillez sélectioner une classe :");
        foreach (var charClass in Enum.GetValues(typeof(Classes))) {
            Console.WriteLine((int)charClass + 1 + " : " + charClass);
        }

        var keyPressed = Console.ReadKey();

        switch (keyPressed.Key) {
            case ConsoleKey.D1:
                selectedClass = Classes.Héros;
                break;
            case ConsoleKey.D2:
                selectedClass = Classes.Mage;
                break;
            case ConsoleKey.D3:
                selectedClass = Classes.Paladin;
                break;
            case ConsoleKey.D4:
                selectedClass = Classes.Noble;
                break;
            case ConsoleKey.D5:
                selectedClass = Classes.Gardien;
                break;
            case ConsoleKey.D6:
                selectedClass = Classes.Berserker;
                break;
            case ConsoleKey.D7:
                selectedClass = Classes.Géant;
                break;
            default:
                isBadClassChoice = true;
                break;
        }
    } while (isBadClassChoice);

    var player = new Player(selectedClass, 20, 5, 5);

    map.createMap();
    map.placeWalls(5, 20);
    map.PlaceExitRandomly(map);

    var monsters = new List<Monster>();
    var randomMonsterNumber = random.Next(1, 5);

    for (var i = 0; i < randomMonsterNumber; i++) {
        var monster = new Monster((MonsterClasses)random.Next(0, 3));
        monster.PlaceMonsterRandomly(map);
        monsters.Add(monster);
    }

    player.PlacePlayerRandomly(map);

    while (!isReset)
    {
        
        
        Console.Clear();
         
        map.displayMap();
        
        player.DisplayPlayerHUD();
        
        player.PlayerActions(ref isReset, ref isRunning, map, ref player, ref monsters, ref etage);
         
    }
}
        }
    }
}



