using System;

namespace Cduc
{
    public enum TileType {
        Ground,
        Wall,
        Heros,
        Bat,
        Goblin,
        Skeleton,
        Exit,
    }

    public class Tile {
        public int posX;
        public int poxY;
        public TileType type;
        public char sprite;
        public ConsoleColor color;
        public bool isPassable;


        public Tile(int posX, int poxY, TileType type) {
            this.posX = posX;
            this.poxY = poxY;

            this.type = type;

            switch (type) {
                case TileType.Ground:
                    sprite = '.';
                    color = ConsoleColor.DarkGray;
                    isPassable = true;
                    break;
                case TileType.Wall:
                    sprite = '#';
                    color = ConsoleColor.Blue;
                    isPassable = false;
                    break;
                case TileType.Heros:
                    sprite = '@';
                    color = ConsoleColor.Red;
                    isPassable = false;
                    break;
                case TileType.Bat:
                    sprite = 'V';
                    color = ConsoleColor.Yellow;
                    isPassable = true;
                    break;
                case TileType.Goblin:
                    sprite = 'G';
                    color = ConsoleColor.Green;
                    isPassable = true;
                    break;
                case TileType.Skeleton:
                    sprite = 'T';
                    color = ConsoleColor.White;
                    isPassable = true;
                    break;
                case TileType.Exit:
                    sprite = 'X';
                    color = ConsoleColor.Yellow;
                    isPassable = false;
                    break;
            }
        }
    }
}