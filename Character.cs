using System;
using System.Collections.Generic;

namespace Cduc
{

   
    public class Character {
    private Statistics statistics;

    public int positionX;
    public int positionY;
    
    public int level;

    public int currentHp;
    public int maxHp;

    public int currentXp;
    public int targetXp;

    public int attack;
    public int defense;

    public Character(Classes playerClasses, int maxHp, int attack, int defense) {
        statistics = new Statistics(playerClasses);
        this.attack = attack;
        this.defense = defense;
        
        level = 1;
        this.maxHp = maxHp;
        currentHp = maxHp;
        currentXp = 0;
        targetXp = 1;
        positionX = 0;
        positionY = 0;
    }
    
    public Character(MonsterClasses monsterClass) {
        statistics = new Statistics(monsterClass);

        switch (monsterClass) {
            case MonsterClasses.Bat:
                maxHp = 5;
                attack = 2;
                defense = 1;
                break;
            case MonsterClasses.Goblin:
                maxHp = 8;
                attack = 2;
                defense = 2;
                break;
            case MonsterClasses.Skeleton:
                maxHp = 10;
                attack = 1;
                defense = 3;
                break;
        }
        
        level = 1;
        currentHp = maxHp;
        currentXp = 0;
        targetXp = 1;
        positionX = 0;
        positionY = 0;
    }

    private void RestoreHp() {
        currentHp = maxHp;
    }

    public void IncreaseHp(int value) {
        currentHp = Math.Min(currentHp + value, maxHp);
    }

    public void DecreaseHp(int value) {
        currentHp = Math.Max(currentHp - value, 0);
    }

    private void DefineTargetXp() {
        targetXp = (int) Math.Round(4 * Math.Pow(level, 3) / 5);
    }

    public static int Attack(int attackVal, int defenseVal)
    {
        var damages = attackVal - defenseVal;
        if (damages < attackVal / 2) damages = attackVal / 2;
        return damages;
    }

    private void LevelUp() {
        level += 1;
        
        statistics.LevelUpStatistic(StatisticType.Hp, ref maxHp);
        RestoreHp();
        
        statistics.LevelUpStatistic(StatisticType.Attack, ref attack);
        statistics.LevelUpStatistic(StatisticType.Defense, ref defense);
        
        DefineTargetXp();
    }

    public void AddExperience(int value) {
        currentXp += value;
        while (currentXp >= targetXp) {
            currentXp -= targetXp;
            LevelUp();
        }
    }

    protected void Move(Map map, int xDirection, int yDirection, ref Player player, ref List<Monster> monsters, ref int etage, ref bool isReset) {
        var futureTile = map.map[positionX + xDirection, positionY + yDirection];

        if (futureTile.type != TileType.Bat && futureTile.type != TileType.Ground &&
            futureTile.type != TileType.Goblin && futureTile.type != TileType.Skeleton && futureTile.type != TileType.Exit) return;
        if (futureTile.type == TileType.Ground)
        {
            map.map[positionX + xDirection, positionY + yDirection] = 
                new Tile(positionX + xDirection, positionY + yDirection, TileType.Heros);
            map.map[positionX, positionY] = new Tile(positionX, positionY, TileType.Ground);
            positionX += xDirection;
            positionY += yDirection;
        }

        if (futureTile.type == TileType.Exit)
        {
            Random random = new Random(); 
            map.createMap();
            map.placeWalls(5, 20);
            map.PlaceExitRandomly(map);
            monsters = new List<Monster>();
            var randomMonsterNumber = random.Next(1, 5);

            for (var i = 0; i < randomMonsterNumber; i++) {
                var monster = new Monster((MonsterClasses)random.Next(0, 3));
                monster.PlaceMonsterRandomly(map);
                for (var j = 0; j < etage; j++)
                {
                    monster.LevelUp();
                }
                Console.WriteLine(monster.level);
                monsters.Add(monster);
            }
            player.PlacePlayerRandomly(map);
            map.displayMap();
            etage++;
            Console.WriteLine("Etage : " + etage);
        }

        if (futureTile.type == TileType.Bat ||
            futureTile.type == TileType.Goblin || futureTile.type == TileType.Skeleton)
        {
            var player1 = player;
            
            var monster = monsters.Find(
                m => m.positionX == player1.positionX + xDirection && m.positionY == player1.positionY + yDirection
            );
            while (monster.currentHp > 0)
            {
                Console.ReadKey();
                monster.DisplayMonsterHUD(monster);
                player.DisplayPlayerHUD();
                if (monster.currentHp > Attack(player.attack, monster.defense))
                {
                    monster.currentHp -= Attack(player.attack, monster.defense);
                }
                player.currentHp -= Attack(monster.attack, player.defense);
                if (player.currentHp <= 0)
                    isReset = true;
                if (monster.currentHp < Attack(player.attack, monster.defense))
                {
                    monster.currentHp = 0;
                    map.map[positionX + xDirection, positionY + yDirection] = 
                        new Tile(positionX + xDirection, positionY + yDirection, TileType.Heros);
                    map.map[positionX, positionY] = new Tile(positionX, positionY, TileType.Ground);
                    positionX += xDirection;
                    positionY += yDirection;
                    monsters.Remove(monster);
                    
                    Console.Clear();
                }
            }
            player.AddExperience(10);
        }
    }
}
}