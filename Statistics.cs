using System;

namespace Cduc
{
  public enum Classes {
    Héros,
    Mage,
    Paladin,
    Noble,
    Gardien,
    Berserker,
    Géant,
}

public enum MonsterClasses {
    Bat,
    Goblin,
    Skeleton,
}

public enum Growth {
    Fast,
    Medium,
    Slow
}

public enum StatisticType {
    Hp,
    Attack,
    Defense,
}

public class Statistics {
    private Random random = new Random();

    private Growth attackGrowth;
    private int attackRollsNumber;
    private int attackFacesNumber;
    
    private Growth defenseGrowth;
    private int defenseRollsNumber;
    private int defenseFacesNumber;
    
    private Growth hpGrowth;
    private int hpRollsNumber;
    private int hpFacesNumber;

    public Statistics(Classes playerClass) {
        switch (playerClass) {
            case Classes.Berserker:
                attackGrowth = Growth.Fast;
                defenseGrowth = Growth.Slow;
                hpGrowth = Growth.Medium;
                break;
            case Classes.Gardien:
                attackGrowth = Growth.Slow;
                defenseGrowth = Growth.Fast;
                hpGrowth = Growth.Medium;
                break;
            case Classes.Géant:
                attackGrowth = Growth.Medium;
                defenseGrowth = Growth.Slow;
                hpGrowth = Growth.Fast;
                break;
            case Classes.Noble:
                attackGrowth = Growth.Medium;
                defenseGrowth = Growth.Fast;
                hpGrowth = Growth.Slow;
                break;
            case Classes.Héros:
                attackGrowth = Growth.Medium;
                defenseGrowth = Growth.Medium;
                hpGrowth = Growth.Medium;
                break;
            case Classes.Mage:
                attackGrowth = Growth.Fast;
                defenseGrowth = Growth.Medium;
                hpGrowth = Growth.Slow;
                break;
            case Classes.Paladin:
                attackGrowth = Growth.Fast;
                defenseGrowth = Growth.Slow;
                hpGrowth = Growth.Medium;
                break;
        }
        
        DefineHpGrowthValues();
        DefineStatisticsGrowthValues(attackGrowth, ref attackRollsNumber,ref attackFacesNumber);
        DefineStatisticsGrowthValues(defenseGrowth, ref defenseRollsNumber, ref defenseFacesNumber);
    }
    
    public Statistics(MonsterClasses monsterClass) {
        switch (monsterClass) {
            case MonsterClasses.Bat:
                attackGrowth = Growth.Medium;
                defenseGrowth = Growth.Slow;
                hpGrowth = Growth.Slow;
                break;
            case MonsterClasses.Goblin:
                attackGrowth = Growth.Slow;
                defenseGrowth = Growth.Slow;
                hpGrowth = Growth.Medium;
                break;
            case MonsterClasses.Skeleton:
                attackGrowth = Growth.Slow;
                defenseGrowth = Growth.Medium;
                hpGrowth = Growth.Slow;
                break;
        }
        
        DefineHpGrowthValues();
        DefineStatisticsGrowthValues(attackGrowth, ref attackRollsNumber,ref attackFacesNumber);
        DefineStatisticsGrowthValues(defenseGrowth, ref defenseRollsNumber, ref defenseFacesNumber);
    }

    private void DefineStatisticsGrowthValues(Growth growth, ref int rollsNumber, ref int facesNumber) {
        switch (growth) {
            case Growth.Fast:
                rollsNumber = 4;
                break;
            case Growth.Medium: 
                rollsNumber = 2;
                break;
            case Growth.Slow:
                rollsNumber = 1;
                break;
        }
        facesNumber = 2;
    }

    private void DefineHpGrowthValues() {
        switch (hpGrowth) {
            case Growth.Fast:
                hpRollsNumber = 9;
                hpFacesNumber = 6;
                break;
            case Growth.Medium: 
                hpRollsNumber = 5;
                hpFacesNumber = 6;
                break;
            case Growth.Slow:
                hpRollsNumber = 3;
                hpFacesNumber = 4;
                break;
        }
    }

    private int RollDice(int rollsNumber, int facesNumber) {
        var count = 0;
        for (var i = 0; i < rollsNumber; i++) {
            count += random.Next(1, facesNumber + 1);
        }
        return count;
    }

    public void LevelUpStatistic(StatisticType type ,ref int targetStatistic) {
        switch (type) {
            case StatisticType.Hp:
                targetStatistic += RollDice(hpRollsNumber, hpFacesNumber);
                break;
            case StatisticType.Attack:
                targetStatistic += RollDice(attackRollsNumber, attackFacesNumber);
                break;
            case StatisticType.Defense:
                targetStatistic += RollDice(defenseRollsNumber, defenseFacesNumber);
                break;
        }
    }
}
}
