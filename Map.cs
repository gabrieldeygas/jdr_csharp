using System;

namespace Cduc
{
    public class Map {
        private Random random = new Random();
        public int mapWidth;
        public int mapHeight;
        public int exitPosX;
        public int exitPosY;
        public Tile[,] map;

        public Map() {
            mapWidth = random.Next(10, 50);
            mapHeight = random.Next(5, 15);
            map = new Tile[mapWidth, mapHeight];
        }

        public void createMap() {
            for (var y = 0; y < mapHeight; y++) {
                for (var x = 0; x < mapWidth; x++) {
                    if (x == 0 || y == 0 || x == mapWidth - 1 || y == mapHeight - 1) {
                        map[x, y] = new Tile(x, y, TileType.Wall);
                    }
                    else map[x, y] = new Tile(x, y, TileType.Ground);
                
                }
            }
        }
    
        public void placeWalls(int minPercent, int maxPercent) {
            var availableNumber = (mapWidth - 2) * (mapHeight - 2);
            float randomPercent = random.Next(minPercent, maxPercent + 1);
            var wallNumber = availableNumber * randomPercent / 100;
        
            for (var i = 0; i < wallNumber; i++) {
                var isPlaced = false;

                while (!isPlaced) {
                    var x = random.Next(0, mapWidth);
                    var y = random.Next(0, mapHeight);

                    if (map[x, y].type == TileType.Ground) {
                        map[x, y] = new Tile(x, y, TileType.Wall);
                        isPlaced = true;
                    }
                }
            }
        }
        
        public void PlaceExitRandomly(Map map) {
            var random = new Random();
            var isPlaced = false;

            while (!isPlaced) {
                var x = random.Next(0, map.mapWidth);
                var y = random.Next(0, map.mapHeight);

                if (map.map[x, y].type == TileType.Ground) {
                    exitPosX = x;
                    exitPosY = y;
                    map.map[x, y] = new Tile(x, y, TileType.Exit);
                    isPlaced = true;
                }
            }
        }

        public void displayMap() {
            for (var y = 0; y < mapHeight; y++) {
                for (var x = 0; x < mapWidth; x++) {
                    Console.ForegroundColor = map[x, y].color;
                    Console.Write(map[x,y].sprite);
                }
                Console.WriteLine();
            }
        }
    }
   
}