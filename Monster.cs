using System;

namespace Cduc
{
    public class Monster : Character {
        private MonsterClasses monsterClass;
        // public int givenXp;
    
        public Monster(MonsterClasses monsterClass) : base(monsterClass) {
            this.monsterClass = monsterClass;
           // givenXp = 
        }
    
        public void PlaceMonsterRandomly(Map map) {
            var random = new Random();
            var isPlaced = false;

            while (!isPlaced) {
                var x = random.Next(0, map.mapWidth);
                var y = random.Next(0, map.mapHeight);

                if (map.map[x, y].type == TileType.Ground) {
                    positionX = x;
                    positionY = y;
                    switch (monsterClass) {
                        case MonsterClasses.Bat:
                            map.map[x, y] = new Tile(x, y, TileType.Bat);
                            break;
                        case MonsterClasses.Goblin:
                            map.map[x, y] = new Tile(x, y, TileType.Goblin);
                            break;
                        case MonsterClasses.Skeleton:
                            map.map[x, y] = new Tile(x, y, TileType.Skeleton);
                            break;
                    }
                
                    isPlaced = true;
                }
            }
        }
        
        public void DisplayMonsterHUD(Monster monster) {
        var hudLength = (" " + monster +
                         " ║ Level : " + level +
                         " ║ HP : " + currentHp + " / " + maxHp +
                         " ║ XP : " + currentXp + " / " + targetXp +
                         " ║ ATT : " + attack +
                         " ║ DEF : " + defense + " ").Length;
        
        Console.Write("Attack : A");
        Console.WriteLine();
        
        Console.ForegroundColor = ConsoleColor.White;
        Console.Write("╔");
        for (var i = 0; i < hudLength; i++) {
            Console.Write("═");
        }
        Console.Write("╗");
        
        Console.WriteLine();
        
        Console.Write("║ ");
        Console.ForegroundColor = ConsoleColor.Cyan;
        Console.Write(monster.monsterClass.ToString());
        Console.ForegroundColor = ConsoleColor.White;
        Console.Write(" ║ ");
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.Write("Level : " + level);
        Console.ForegroundColor = ConsoleColor.White;
        Console.Write(" ║ ");
        Console.ForegroundColor = ConsoleColor.Green;
        Console.Write("HP : " + currentHp + " / " + maxHp);
        Console.ForegroundColor = ConsoleColor.White;
        Console.Write(" ║ ");
        Console.ForegroundColor = ConsoleColor.Magenta;
        Console.Write("XP : " + currentXp + " / " + targetXp);
        Console.ForegroundColor = ConsoleColor.White;
        Console.Write(" ║ ");
        Console.ForegroundColor = ConsoleColor.Red;
        Console.Write("ATT : " + attack);
        Console.ForegroundColor = ConsoleColor.White;
        Console.Write(" ║ ");
        Console.ForegroundColor = ConsoleColor.Blue;
        Console.Write("DEF : " + defense);
        Console.ForegroundColor = ConsoleColor.White;
        Console.Write(" ║");
        
        Console.WriteLine();
        
        Console.Write("╚");
        for (var i = 0; i < hudLength; i++) {
            Console.Write("═");
        }
        Console.Write("╝");
    }
    }
}